# Software Engineering

## Gitlab CICD
1. Started with the Gitlab Docs[Introduction to CI/CD with GitLab](https://docs.gitlab.com/ee/ci/introduction/)
2. Key steps 
..1. Have pplication codebase hosted in a Git repository
..2. Specified in a configuration file called [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/README.html),
located in the root path of your repository.
..3. In the file, define the scripts would like to run
as the commands need to be on terminal
..4. After adding config file .gitlab-ci.yml, Gitlab would
detect it and run your scripts with the tool called [GitLab Runner](https://docs.gitlab.com/runner/),
..5. The scripts are grouped into jobs, and together they compose a pipeline.
..6. Script is run, get pipeline feedback and execution result
..7. If something unexpected happens, do a roll back

### .gitlab-ci.yml
* A minimalist example of .gitlab-ci.yml file could contain:
```
before_script: #install dependencies for app
  - apt-get install rubygems ruby-dev -y

run-test: #print Ruby version of current system
  script:
    - ruby --version
```
* Both of them compose a pipeline triggered at every push 
to any branch of the repository.
* pipeline status and execution feedback is given

### Roll back
* At the end, if anything goes wrong, you can easily roll back all the changes:
..* [Roll back](https://docs.gitlab.com/ee/ci/environments/index.html#retrying-and-rolling-back)

### advance features
* Verify, Package, Release
![alt text][gitlab CICD]
[gitlab CICD]: https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_extended_v12_3.png
